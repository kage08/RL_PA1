import numpy as np
import matplotlib.pyplot as plt
from bandit import BanditProblem

def testbed_initialize(count = 2000, mu=0, sigma=1, arms=10):
	problems = [BanditProblem(arms, mu, sigma) for x in range(count)]
	return problems


def epsilon_action(epsilon , bandit_problem):
	assert (epsilon>=0 and epsilon<=1 and bandit_problem is not None), "Wrong value for epsilon"

	#Toss a coin with probability of heads(1) being epsilon
	toss = np.random.binomial(1,epsilon)
	#print('toss:',toss)

	#Head: select a arm UAR
	if toss==1:
		arm = np.random.randint(0, bandit_problem.k)
	#Select Argmax Q(a)
	else:
		arm = bandit_problem.get_current_max()

	return bandit_problem.select_action(arm), arm



def perform_epsilon_test(epsilon, steps, count):
	
	

	testbed = testbed_initialize(count=count)

	average_reward = []

	optimal_percent = []

	for i in range(steps):
		count_optimal = 0
		total_reward = 0.0

		for bandit_ in testbed:
			
			#Choose by epsilon greedy strategy
			reward, arm_chosen = epsilon_action(epsilon, bandit_)

			total_reward += reward

			if arm_chosen == bandit_.optimal_action: count_optimal+=1

		average_reward.append(total_reward/count)
		optimal_percent.append(count_optimal/count)

	return average_reward, optimal_percent



if __name__ == '__main__':
	
	steps = 1000
	count = 2000
	rewards = []
	opt_percent = []

	#Get data for epsilon =0, 0.01, 0.1

	avg_reward, opt_percentage = perform_epsilon_test(0.0, steps, count)
	rewards.append(avg_reward)
	opt_percent.append(opt_percentage)

	avg_reward, opt_percentage = perform_epsilon_test(0.01, steps, count)
	rewards.append(avg_reward)
	opt_percent.append(opt_percentage)

	avg_reward, opt_percentage = perform_epsilon_test(0.1, steps, count)
	rewards.append(avg_reward)
	opt_percent.append(opt_percentage)

	avg_reward, opt_percentage = perform_epsilon_test(0.3, steps, count)
	rewards.append(avg_reward)
	opt_percent.append(opt_percentage)

	#Plot graphs for avrage reward and fraction of tasks choosing optimal arm
	plt.title('Comparison of average rewards using epsilon-greedy')
	plt.xlabel('Steps')
	plt.ylabel('Average reward')

	plt.plot([x+1 for x in range(steps)], rewards[0], label='0.0')
	plt.plot([x+1 for x in range(steps)], rewards[1], label='0.01')
	plt.plot([x+1 for x in range(steps)], rewards[2], label='0.1')
	plt.plot([x+1 for x in range(steps)], rewards[3], label='0.3')
	plt.legend()

	plt.savefig('q1_avg.png')
	plt.show()

	plt.clf()

	plt.title('Comparison of % optimal reward using epsilon-greedy')
	plt.xlabel('Steps')
	plt.ylabel('Fraction of tasks choosing optimal action')

	plt.plot([x+1 for x in range(steps)],opt_percent[0], label='0.0')
	plt.plot([x+1 for x in range(steps)],opt_percent[1], label='0.01')
	plt.plot([x+1 for x in range(steps)],opt_percent[2], label='0.1')
	plt.plot([x+1 for x in range(steps)],opt_percent[3], label='0.3')
	plt.legend()

	plt.savefig('q1_opt.png')
	plt.show()
