import numpy as np
import matplotlib.pyplot as plt
from bandit import BanditProblem

def testbed_initialize(count = 2000, mu=0, sigma=1, arms=10):
	problems = [BanditProblem(arms, mu, sigma) for x in range(count)]
	return problems



def perform_softmax_selection(steps, count, temp):

	testbed = testbed_initialize(count)
	average_reward = []

	optimal_percent = []

	for i in range(steps):
		count_optimal = 0
		total_reward = 0.0

		for bandit_ in testbed:

			
			#Select by gibbs distribution on Q
			reward, arm = bandit_.select_action_softmax(temp)
			
			total_reward += reward
			
			if arm == bandit_.optimal_action: count_optimal+=1

		average_reward.append(total_reward/count)
		optimal_percent.append(count_optimal/count)

	return average_reward, optimal_percent


if __name__ == '__main__':
	
	steps = 1000
	count = 2000
	rewards = []
	opt_percent = []

	#Get data for epsilon =0, 0.01, 0.1

	avg_reward, opt_percentage = perform_softmax_selection(steps, count, 0.01)
	rewards.append(avg_reward)
	opt_percent.append(opt_percentage)

	avg_reward, opt_percentage = perform_softmax_selection(steps, count, 0.1)
	rewards.append(avg_reward)
	opt_percent.append(opt_percentage)

	avg_reward, opt_percentage = perform_softmax_selection(steps, count, 0.25)
	rewards.append(avg_reward)
	opt_percent.append(opt_percentage)

	avg_reward, opt_percentage = perform_softmax_selection(steps, count, 1)
	rewards.append(avg_reward)
	opt_percent.append(opt_percentage)

	avg_reward, opt_percentage = perform_softmax_selection(steps, count, 10)
	rewards.append(avg_reward)
	opt_percent.append(opt_percentage)

	#Plot graphs for avrage reward and fraction of tasks choosing optimal arm
	plt.title('Average rewards using Softmax selection for different temperatures')
	plt.xlabel('Steps')
	plt.ylabel('Average reward')

	plt.plot([x+1 for x in range(steps)], rewards[0], label='0.01')
	plt.plot([x+1 for x in range(steps)], rewards[1], label='0.1')
	plt.plot([x+1 for x in range(steps)], rewards[2], label='0.25')
	plt.plot([x+1 for x in range(steps)], rewards[3], label='1')
	plt.plot([x+1 for x in range(steps)], rewards[4], label='10')
	plt.legend()

	plt.savefig('q2_avg.png')
	plt.show()

	plt.clf()

	plt.title(' fraction of optimal reward using Softmax selection for different temperatures')
	plt.xlabel('Steps')
	plt.ylabel('Fraction of tasks choosing optimal action')

	plt.plot([x+1 for x in range(steps)],opt_percent[0], label='0.01')
	plt.plot([x+1 for x in range(steps)],opt_percent[1], label='0.1')
	plt.plot([x+1 for x in range(steps)],opt_percent[2], label='0.25')
	plt.plot([x+1 for x in range(steps)],opt_percent[3], label='1')
	plt.plot([x+1 for x in range(steps)],opt_percent[4], label='10')
	plt.legend()

	plt.savefig('q2_opt.png')
	plt.show()
