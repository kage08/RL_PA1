import numpy as np
import matplotlib.pyplot as plt
from bandit import BanditProblem
from copy import deepcopy

def testbed_initialize(count = 2000, mu=0, sigma=1, arms=10):
	problems = [BanditProblem(arms, mu, sigma) for x in range(count)]
	return problems

def median_elim(epsilon, delta, bandit_problem):
	assert(bandit_problem is not None), 'Wrong input bandit'

	#Set of arms to consider at each iteration
	arm_set = set(range(bandit_problem.k))

	#initialize epsilon_t, delta_t
	ep_t = epsilon/4
	delta_t = delta/2

	total_sample_time = 0

	while len(arm_set)>1:
		#Compute number of samples to draw from each arm
		sample_time = np.square(2/ep_t)*np.log(3/delta_t)
		total_sample_time += sample_time
		print(total_sample_time)

		#bandit_problem.refresh()

		#Draw samples from each arm
		for arm in arm_set:
			for i in range(int(sample_time)):
				bandit_problem.select_action(arm)

		#Collect reward estimates for relevant bandits which are in arm_set
		rewards = [bandit_problem.Q_t[i] for i in arm_set]

		#Find the median
		median = np.median(rewards)

		temp_set = set()
		for arm in arm_set:

			#Find all arms to eliminate that give less than median estimates
			if bandit_problem.Q_t[arm] < median:
				temp_set.add(arm)

		#Update by removing eliminated arms
		arm_set = arm_set - temp_set

		#Update epsilon_t, delta_t
		ep_t *= 0.75
		delta_t /= 2

	#Return the left over arm
	return arm_set.pop()

if __name__ == '__main__':
	bandit_ = BanditProblem(k=9)
	print(bandit_.optimal_action)
	print(bandit_.action_values)
	print(median_elim(0.1,0.1,bandit_))

	count = 500

	epsilon = 0.1
	delta = 0.1

	print('epsilon:', epsilon)
	print('delta:', delta)

	optimal_count = 0
	epsilon_optimal_count = 0

	testbed = testbed_initialize(count=count)

	for bandit_ in testbed:
		chosen_arm = median_elim(epsilon, delta, bandit_)
		chosen_arm_value = bandit_.action_values[chosen_arm]
		optimal_arm = bandit_.optimal_action
		optimal_arm_value = bandit_.optimal_action_value()

		if chosen_arm == optimal_arm: optimal_count += 1
		if (optimal_action_value - chosen_arm_value) <= epsilon : epsilon_optimal_count+=1
		print('Done')

	print('Fraction choosing optimal arm:', optimal_count/count)
	print('Fraction choosing epsilon optimal arm', epsilon_optimal_count/count)



