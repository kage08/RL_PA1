import numpy as np
import matplotlib.pyplot as plt

#Class for multi-arm bandit problem
class BanditProblem:

	#init function contains all parameters of single bandit problem
	def __init__(self, k=10, action_mean=0, action_variance=1, softmax = False, alpha=0.1):
		self.k = k
		self.action_mean, self.action_variance = action_mean, action_variance

		#initialize action values from normal distribution
		self.action_values = np.random.normal(self.action_mean, self.action_variance, size=(self.k))

		#initialize Qt
		self.Q_t = np.zeros((self.k))

		#initialize Nt : number of times a particular action was taken
		self.N_t = np.zeros((self.k))

		self.optimal_action = np.argmax(self.action_values)
		self.optimal_action_value = np.max(self.action_values)

		#Reward at next current time
		self.R_t = 0
		self.time = 0

		#for softmax
		self.softmax = softmax

		#For gradient
		self.H_t = np.zeros((self.k))
		self.alpha = alpha

		#Mean of rewards: Used for softmax
		self.R_mean = 0





	#Select a action and do corresponding updates
	def select_action(self, action):
		#check if correct action
		assert(action>=0 and action < self.k), "No such action"

		self.N_t[action]+=1
		self.time+= 1

		#sample reward from a normal distribution centered around q*
		self.R_t=np.random.normal(self.action_values[action], 1)


		if self.softmax:

			self.R_mean += ((1/self.time)*(self.R_t-self.R_mean))

		

		self.update_Q(action, self.R_t)

		return self.R_t





	#Update Q value of action
	def update_Q(self, action, reward):
		self.Q_t[action] += ((1/self.N_t[action])*(reward - self.Q_t[action]))

	#Get Argmax Q(a)
	def get_current_max(self):
		return np.argmax(self.Q_t)

	#Get Argmax Q(a) + c*sqrt(log(t)/N(a))
	def get_current_max_UCB(self, c=1):
		if 0 in self.N_t:
			return np.where(self.N_t==0)[0][0]
		else:
			return np.argmax(self.Q_t + c*np.sqrt(np.log(self.time+1) / self.N_t))





	#Select based on Gibbs Distribution on Q_t
	def select_action_softmax(self, temp):
		self.temp = temp

		#Get probability distribution by softmax
		self.distribution = np.exp(self.Q_t/self.temp)/np.sum(np.exp(self.Q_t/self.temp))

		#Sample from above distribution
		arm = np.random.choice(np.arange(self.k), p = self.distribution)
		reward = self.select_action(arm)

		return reward, arm




	#FOLLOWING CODE is for grdient based algo described in book.
	def select_action_softmax_gradient(self, temp):
		self.temp=temp
		self.distribution = np.exp(self.H_t/self.temp)/np.sum(np.exp(self.H_t/self.temp))
		arm = np.random.choice(np.arange(self.k), p = self.distribution)
		reward = self.select_action(arm)

		self.update_preference_gradient(arm)

		return reward, arm

	def update_preference_gradient(self, arm):
		for i in range(self.k):
			if arm == i:
				self.H_t[i] += self.alpha*(self.R_t-self.R_mean)*(1-self.distribution[i])
			else:
				self.H_t[i] -= self.alpha*(self.R_t-self.R_mean)*self.distribution[i]





	#Refresh Q(a) N(a) t=0  (a helper function)
	def refresh(self):
		#initialize Qt
		self.Q_t = np.zeros((self.k))

		#initialize Nt : number of times a particular action was taken
		self.N_t = np.zeros((self.k))

		self.R_t = 0
		self.time = 0

		self.R_mean = 0

