import numpy as np
import matplotlib.pyplot as plt
from bandit import BanditProblem
from epsilon_greedy import perform_epsilon_test
from softmax_selection import perform_softmax_selection
from UCB import perform_UCB_test

if __name__ == '__main__':
	
	steps = 1000
	count = 2000
	rewards = []
	opt_percent = []

	vg_reward, opt_percentage = perform_epsilon_test(0.1, steps, count)
	rewards.append(avg_reward)
	opt_percent.append(opt_percentage)

	avg_reward, opt_percentage = perform_softmax_selection(steps, count, 0.25)
	rewards.append(avg_reward)
	opt_percent.append(opt_percentage)

	avg_reward, opt_percentage = perform_UCB_test(1, steps, count)
	rewards.append(avg_reward)
	opt_percent.append(opt_percentage)

	plt.title('Comparison of Average rewards')
	plt.xlabel('Steps')
	plt.ylabel('Average reward')

	plt.plot([x+1 for x in range(steps)], rewards[0], label='e-greedy')
	plt.plot([x+1 for x in range(steps)], rewards[1], label='softmax')
	plt.plot([x+1 for x in range(steps)], rewards[2], label='UCB')
	
	
	plt.legend()

	plt.savefig('q3b_avg.png')
	plt.show()

	plt.clf()

	plt.title('Comparison of % optimal reward')
	plt.xlabel('Steps')
	plt.ylabel('Fraction of tasks choosing optimal action')

	plt.plot([x+1 for x in range(steps)],opt_percent[0], label='e-greedy')
	plt.plot([x+1 for x in range(steps)],opt_percent[1], label='softmax')
	plt.plot([x+1 for x in range(steps)],opt_percent[2], label='UCB')
	
	plt.legend()

	plt.savefig('q3b_opt.png')
	plt.show()