import numpy as np
import matplotlib.pyplot as plt
from bandit import BanditProblem


def testbed_initialize(count = 2000, mu=0, sigma=1, arms=10):
	problems = [BanditProblem(arms, mu, sigma) for x in range(count)]
	return problems


def perform_UCB_test(c, steps, count):
	
	

	testbed = testbed_initialize(count=count)

	average_reward = []

	optimal_percent = []

	for i in range(steps):
		count_optimal = 0
		total_reward = 0.0

		for bandit_ in testbed:

			#Get argmax Q + confidence term
			arm = bandit_.get_current_max_UCB(c=c)
			reward = bandit_.select_action(arm)

			total_reward += reward
			if arm == bandit_.optimal_action: count_optimal+=1

		average_reward.append(total_reward/count)
		optimal_percent.append(count_optimal/count)

	return average_reward, optimal_percent


if __name__ == '__main__':
	
	steps = 1000
	count = 2000
	rewards = []
	opt_percent = []

	#Get data for epsilon =0, 0.01, 0.1

	avg_reward, opt_percentage = perform_UCB_test(1, steps, count)
	rewards.append(avg_reward)
	opt_percent.append(opt_percentage)

	avg_reward, opt_percentage = perform_UCB_test(5, steps, count)
	rewards.append(avg_reward)
	opt_percent.append(opt_percentage)

	avg_reward, opt_percentage = perform_UCB_test(10, steps, count)
	rewards.append(avg_reward)
	opt_percent.append(opt_percentage)

	avg_reward, opt_percentage = perform_UCB_test(0.5, steps, count)
	rewards.append(avg_reward)
	opt_percent.append(opt_percentage)

	avg_reward, opt_percentage = perform_UCB_test(2, steps, count)
	rewards.append(avg_reward)
	opt_percent.append(opt_percentage)

	#Plot graphs for avrage reward and fraction of tasks choosing optimal arm
	plt.title('Average rewards using UCB with different c values')
	plt.xlabel('Steps')
	plt.ylabel('Average reward')

	plt.plot([x+1 for x in range(steps)], rewards[0], label='1')
	plt.plot([x+1 for x in range(steps)], rewards[3], label='2')
	plt.plot([x+1 for x in range(steps)], rewards[1], label='5')
	plt.plot([x+1 for x in range(steps)], rewards[2], label='10')
	plt.plot([x+1 for x in range(steps)], rewards[3], label='0.5')
	
	plt.legend()

	plt.savefig('q3_avg.png')
	plt.show()

	plt.clf()

	plt.title('% optimal reward using UCB with different c values')
	plt.xlabel('Steps')
	plt.ylabel('Fraction of tasks choosing optimal action')

	plt.plot([x+1 for x in range(steps)],opt_percent[0], label='1')
	plt.plot([x+1 for x in range(steps)],opt_percent[3], label='2')
	plt.plot([x+1 for x in range(steps)],opt_percent[1], label='5')
	plt.plot([x+1 for x in range(steps)],opt_percent[2], label='10')
	plt.plot([x+1 for x in range(steps)],opt_percent[3], label='0.5')

	plt.legend()

	plt.savefig('q3_opt.png')
	plt.show()
