import numpy as np
import matplotlib.pyplot as plt
from bandit import BanditProblem
from copy import deepcopy

def testbed_initialize(count = 2000, mu=0, sigma=1, arms=10):
	problems = [BanditProblem(arms, mu, sigma) for x in range(count)]
	return problems

def median_elim(epsilon, delta, bandit_problem):
	assert(bandit_problem is not None), 'Wrong input bandit'

	#Set of arms to consider at each iteration
	arm_set = set(range(bandit_problem.k))

	#initialize epsilon_t, delta_t
	ep_t = epsilon/4
	delta_t = delta/2

	total_sample_time = 0

	reward_list = []

	while len(arm_set)>1:
		#Compute number of samples to draw from each arm
		sample_time = np.square(2/ep_t)*np.log(3/delta_t)

		#bandit_problem.refresh()

		#Draw samples from each arm
		for arm in arm_set:
			for i in range(int(sample_time)):
				reward_list.append(bandit_problem.select_action(arm))

		#Collect reward estimates for relevant bandits which are in arm_set
		rewards = [bandit_problem.Q_t[i] for i in arm_set]

		#Find the median
		median = np.median(rewards)

		temp_set = set()
		for arm in arm_set:

			#Find all arms to eliminate that give less than median estimates
			if bandit_problem.Q_t[arm] < median:
				temp_set.add(arm)

		#Update by removing eliminated arms
		arm_set = arm_set - temp_set

		#Update epsilon_t, delta_t
		ep_t *= 0.75
		delta_t /= 2

	print(total_sample_time)

	#Return the left over arm
	return arm_set.pop(), reward_list

if __name__ == '__main__':
	count = 500
	epsilon = 0.1
	delta = 0.1

	testbed = testbed_initialize(count=count)

	ct = 0

	mean_rewards = []

	for bandit_ in testbed:
		_, rewards = median_elim(epsilon, delta, bandit_)

		if ct == 0:
			mean_rewards = rewards[ :]
		else:
			for i in range(len(rewards)):
				mean_rewards[i] += (1/(ct+1))*(rewards[i]-mean_rewards[i])

		ct += 1
		print('Done:',ct)

	filename = 'Median_rewards.txt'
	f = open(filename, 'w')
	f.write(str(mean_rewards))


	








